import React, { createContext, useState } from "react";

export const CharStats = createContext({
  name: "Geralt",
  str: 0,
  hp: 0,
  speed: 0,
  addStr: (item) => {},
  changeName: (newName) => {},
});

export const UserStatsProvider = ({ children }) => {
  const [name, changeName] = useState("Luffy");
  const [str, addStr] = useState(parseInt(localStorage.getItem("strRand")));
  const hp = localStorage.getItem("hpRand");
  const speed = localStorage.getItem("spdRand");

  return (
    <CharStats.Provider
      value={{
        name,
        str,
        hp,
        speed,
        addStr,
        changeName,
      }}
    >
      {children}
    </CharStats.Provider>
  );
};
