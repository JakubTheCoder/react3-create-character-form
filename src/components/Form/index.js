import React from "react";
import User from "../../components/User";
import { UserStatsProvider } from "../../context";

class Form extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: "",
      showStats: false,
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(e) {
    this.setState({ value: e.target.value });
  }

  handleSubmit(e) {
    localStorage.clear();
    e.preventDefault();
    localStorage.setItem("name", this.state.value);

    var min = Math.ceil(1);
    var max = Math.floor(15);
    var rand = Math.floor(min + Math.random() * (max - min));
    localStorage.setItem("strRand", rand);

    min = Math.ceil(50);
    max = Math.floor(200);
    rand = Math.floor(min + Math.random() * (max - min));
    localStorage.setItem("hpRand", rand);

    min = Math.ceil(1);
    max = Math.floor(7);
    rand = Math.floor(min + Math.random() * (max - min));
    localStorage.setItem("spdRand", rand);

    this.setState({ showStats: true });
  }

  render() {
    return this.state.showStats ? (
      <div>
        {
          <UserStatsProvider>
            <User level="10" />
          </UserStatsProvider>
        }
      </div>
    ) : (
      <div>
        <form onSubmit={this.handleSubmit}>
          <label>Name:</label>
          <input
            name="name"
            type="text"
            value={this.state.value}
            onChange={this.handleChange}
          />
          <input name="send" type="submit" />
        </form>
      </div>
    );
  }
}

export default Form;
