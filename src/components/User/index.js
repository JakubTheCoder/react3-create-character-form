import { useContext } from "react";
import { CharStats } from "../../context";

import "./style.css";

const User = (props) => {
  var { name, str, hp, speed, addStr, changeName } = useContext(CharStats);
  changeName(localStorage.getItem("name"));
  return (
    <div className="col-6">
      <div>
        <h1>{props.level}</h1>
        <p>Name: {name}</p>
        <p>str: {str}</p>
        <p>hp:{hp}</p>
        <p>speed: {speed}</p>

        <div onClick={() => addStr(str + 1)}> Add +1 str</div>
      </div>
    </div>
  );
};

export default User;
