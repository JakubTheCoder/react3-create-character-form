import "./App.css";
import Homepage from "./pages/homepage";

function App() {
  return (
    <div className="App">
      <Homepage title="EasyCode" />
    </div>
  );
}

export default App;
